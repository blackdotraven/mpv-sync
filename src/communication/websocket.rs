use async_trait::async_trait;
use async_tungstenite::{
    tokio::{connect_async, ConnectStream},
    tungstenite::protocol::Message,
    WebSocketStream,
};
use failure::{format_err, Error};
use futures::{
    stream::Stream,
    task::{Context, Poll},
};
use futures::{SinkExt, StreamExt};
use std::{pin::Pin, result::Result};
use url::Url;

use super::super::communication;

pub struct Communication {
    ws_stream: Option<WebSocketStream<ConnectStream>>,
}

#[async_trait]
impl communication::Communication for Communication {
    async fn new() -> Result<Box<Self>, Error> {
        Ok(Box::new(Self { ws_stream: None }))
    }

    async fn open(&mut self, uri: &str) -> Result<(), Error> {
        assert!(self.ws_stream.is_none(), "Already open");

        let url = Url::parse(&uri)?;

        let (ws_stream, _) = connect_async(url).await?;
        self.ws_stream = Some(ws_stream);

        Ok(())
    }

    async fn send(&mut self, message: communication::Message) -> Result<(), Error> {
        debug!("Sending message: {:?}", message);

        // TODO Add handling of closed connections (auto reconnect)
        self.ws_stream
            .as_mut()
            .unwrap()
            .send(Message::Text(message.to_json()?))
            .await
            .map_err(|error| format_err!("async_tungstenite::tungstenite::Error: {}", error))
    }

    async fn close(self) -> Result<(), Error> {
        self.ws_stream
            .unwrap()
            .close(None)
            .await
            .map_err(|error| format_err!("async_tungstenite::tungstenite::Error: {}", error))
    }
}

impl Stream for Communication {
    type Item = Result<communication::Message, Error>;

    fn poll_next(mut self: Pin<&mut Self>, context: &mut Context) -> Poll<Option<Self::Item>> {
        self.ws_stream
            .as_mut()
            .unwrap()
            .poll_next_unpin(context)
            .map(|msg| {
                // TODO Add handling of closed connections (auto reconnect)
                let raw_message = msg.unwrap().unwrap().to_string();
                debug!("Received message: {:?}", raw_message);
                Some(communication::parse_json_message(&raw_message))
            })
    }
}
