use failure::{format_err, Error};
use sntpc;
use std::{
    sync::{Arc, Mutex},
    thread, time,
    time::{SystemTime, UNIX_EPOCH},
};

lazy_static! {
    static ref OFFSET: Arc<Mutex<Option<f64>>> = Arc::new(Mutex::new(None));
}

pub fn init_with_offset(offset: f64) {
    let mut offset_option = OFFSET.lock().unwrap();
    *offset_option = Some(offset);
}

pub fn init(count_min: i64, server: String) -> Result<(), Error> {
    let mut count = 0;

    let mut offset_option = OFFSET.lock().unwrap();
    *offset_option = Some(0.0);
    let offset = offset_option.as_mut().unwrap();

    while count < count_min {
        *offset = 1.0 as f64 / (count + 1) as f64
            * ((*offset * count as f64)
                + loop {
                    let result = sntpc::request(&server, 123);

                    match result {
                        Ok(time) => {
                            assert_ne!(time.sec(), 0);
                            let ntp_time: f64 = format!("{}.{}", time.sec(), time.nsec())
                                .parse::<f64>()
                                .unwrap();

                            let system_time = SystemTime::now()
                                .duration_since(UNIX_EPOCH)
                                .expect("Time went backwards")
                                .as_secs_f64();

                            break ntp_time - system_time;
                        }
                        Err(err) => warn!("Err: {}", err.to_string()),
                    }

                    thread::sleep(time::Duration::from_secs_f64(1.0));
                });

        debug!(
            "NTP sync: count: {:?}/{:?}, offset: {:?}",
            count + 1,
            count_min,
            offset
        );

        count += 1;

        thread::sleep(time::Duration::from_secs_f64(0.1));
    }
    Ok(())
}

pub fn get_offset() -> Result<f64, Error> {
    OFFSET
        .lock()
        .unwrap()
        .ok_or(format_err!("offset is not set"))
}

pub fn get_timestamp() -> Result<f64, Error> {
    Ok(SystemTime::now()
        .duration_since(UNIX_EPOCH)
        .expect("Time went backwards")
        .as_secs_f64()
        + OFFSET
            .lock()
            .unwrap()
            .ok_or(format_err!("offset is not set"))?)
}
